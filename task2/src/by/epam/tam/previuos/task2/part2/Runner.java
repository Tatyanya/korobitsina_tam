package by.epam.tam.previuos.task2.part2;

public class Runner {

	public static void main(String[] args) {
		int numberValue, valueAdd, valueSearch;

		Console.out("Enter number of values in collections");
		numberValue = Console.intScanner();

		Console.out("Enter value for adding");
		valueAdd = Console.intScanner();

		valueSearch = (int) (Math.random() * numberValue);
		Console.out("Index for search is " + valueSearch + "\n");

		ArrayListCollection arrayList = new ArrayListCollection();
		arrayList.performAction(numberValue, valueAdd, valueSearch);

		LinkedListCollection linkedList = new LinkedListCollection();
		linkedList.performAction(numberValue, valueAdd, valueSearch);

		HashSetCollection hashSet = new HashSetCollection();
		hashSet.performAction(numberValue, valueAdd, valueSearch);

		TreeSetCollection treeSet = new TreeSetCollection();
		treeSet.performAction(numberValue, valueAdd, valueSearch);

		HashMapCollection hashMap = new HashMapCollection();
		hashMap.performAction(numberValue, valueAdd, valueSearch);

		TreeMapCollection treeMap = new TreeMapCollection();
		treeMap.performAction(numberValue, valueAdd, valueSearch);
	}
}