package by.epam.tam.previuos.task2.part2;

import java.util.ArrayList;
import java.util.TreeSet;

public class TreeSetCollection extends AbstractCollection {
	private TreeSet<Integer> treeSet;

	public TreeSetCollection() {
		treeSet = new TreeSet<Integer>();
	}

	@Override
	public void fillCollection(int value) {
		for (int i = 0; i < value; i++) {
			Integer d = (int) (Math.random() * Integer.MAX_VALUE);
			treeSet.add(d);
		}
	}

	@Override
	public long addToEndOfCollection(int value) {
		long startTime = System.nanoTime();
		treeSet.add(value);

		return System.nanoTime() - startTime;
	}

	@Override
	public long removeLastFromCollection() {
		long startTime = System.nanoTime();
		if (treeSet.size() > 0) {
			treeSet.remove(treeSet.size() - 1);
		} else {
			Console.out("Size of collection is " + treeSet.size());
			return -1;
		}
		return System.nanoTime() - startTime;
	}

	@Override
	public long searchElementByIndex(int searchIndex) {
		long startTime = System.nanoTime();
		try {
			new ArrayList<Integer>(treeSet).get(searchIndex);

			return System.nanoTime() - startTime;
		} catch (Exception e) {
			Console.out("Size of collection " + getClass().getName() + " and using method "
					+ Thread.currentThread().getStackTrace()[1].getMethodName() + " is " + treeSet.size());
		}
		return 0;

	}

	@Override
	public void performAction(int numberValue, int valueAdd, int valueSearch) {
		// TreeSet
		this.fillCollection(numberValue);
		// add
		long addTimeToTheEndTime = this.addToEndOfCollection(valueAdd);

		// remove
		long removeLastTime = this.removeLastFromCollection();

		long searchTime = this.searchElementByIndex(valueSearch);

		Console.out("treeSet:\n" + "Add to the end:\t" + addTimeToTheEndTime + "ns\n" + "Search element: " + searchTime
				+ "ns\n" + "Remove from the end:\t" + removeLastTime + "ns\n");
	}
}