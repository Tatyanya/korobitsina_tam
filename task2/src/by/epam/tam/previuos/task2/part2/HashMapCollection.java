package by.epam.tam.previuos.task2.part2;

import java.util.HashMap;

public class HashMapCollection extends AbstractCollection {
	private HashMap<Integer, Integer> hashMap = new HashMap<Integer, Integer>();

	@Override
	public void fillCollection(int value) {
		for (int i = 0; i < value; i++) {
			Integer d = (int) (Math.random() * Integer.MAX_VALUE);
			hashMap.put(i, d);
		}
	}

	@Override
	public long addToEndOfCollection(int value) {
		long startTime = System.nanoTime();
		hashMap.put(hashMap.size(), value);
		return System.nanoTime() - startTime;
	}

	@Override
	public long removeLastFromCollection() {
		long startTime = System.nanoTime();

		if (hashMap.size() > 0) {
			hashMap.remove(hashMap.size() - 1);
		} else {
			Console.out("Size of collection is " + hashMap.size());
			return -1;

		}

		return System.nanoTime() - startTime;
	}

	@Override
	public long searchElementByIndex(int searchIndex) {
		long startTime = System.nanoTime();

		if (hashMap.size() > 0) {
			hashMap.get(searchIndex);
			return System.nanoTime() - startTime;

		} else {
			Console.out("Size of collection " + getClass().getName() + " and using method "
					+ Thread.currentThread().getStackTrace()[1].getMethodName() + " is " + hashMap.size());
			return 0;
		}
	}

	@Override
	public void performAction(int numberValue, int valueAdd, int valueSearch) {
		// HashMap
		this.fillCollection(numberValue);
		// add
		long addTimeToTheEndTime = this.addToEndOfCollection(valueAdd);

		// remove
		long removeLastTime = this.removeLastFromCollection();

		long searchTime = this.searchElementByIndex(valueSearch);

		Console.out("HashMap:\n" + "Add to the end:\t" + addTimeToTheEndTime + "ns\n" + "Search element: " + searchTime
				+ "ns\n" + "Remove from the end:\t" + removeLastTime + "ns\n");
	}

}