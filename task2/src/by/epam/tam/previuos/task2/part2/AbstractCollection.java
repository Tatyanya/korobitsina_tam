package by.epam.tam.previuos.task2.part2;

public abstract class AbstractCollection {

    public abstract void fillCollection(int value);

    public abstract long addToEndOfCollection(int element);
    public abstract long removeLastFromCollection();
    public abstract long searchElementByIndex(int searchIndex);
    public abstract void performAction(int numberValue, int valueAdd, int valueSearch);

}
