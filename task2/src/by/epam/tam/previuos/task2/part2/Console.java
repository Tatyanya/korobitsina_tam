package by.epam.tam.previuos.task2.part2;

import java.util.Scanner;

public class Console {

    private static Scanner in;


    public static int intScanner() {
        in = new Scanner(System.in);
        int intValue = 0;
        boolean a = true;
        while (a) {
            try {
                intValue = in.nextInt();
                a = false;
            } catch (Throwable exc) {
                System.out.println("Error: only Integer can be used");
                in.nextLine();
            }
        }
        return intValue;
    }

    public static void out(String textPrint) {
        System.out.println(textPrint);
    }

}

