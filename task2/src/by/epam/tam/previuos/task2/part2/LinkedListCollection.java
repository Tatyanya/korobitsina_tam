package by.epam.tam.previuos.task2.part2;

import java.util.LinkedList;
import java.util.NoSuchElementException;

public class LinkedListCollection extends AbstractCollection {
	private LinkedList<Integer> linkedList;

	public LinkedListCollection() {
		this.linkedList = new LinkedList<Integer>();
	}

	@Override
	public void fillCollection(int value) {
		for (int i = 0; i < value; i++) {
			Integer d = (int) (Math.random() * Integer.MAX_VALUE);
			linkedList.add(d);
		}
	}

	@Override
	public long addToEndOfCollection(int value) {
		long startTime = System.nanoTime();
		linkedList.addLast(value);

		return System.nanoTime() - startTime;
	}

	public long addToTheBeginningOfCollection(int value) {
		long startTime = System.nanoTime();
		linkedList.addFirst(value);

		return System.nanoTime() - startTime;
	}

	public long addToMiddleOfCollection(int value) {
		long startTime = System.nanoTime();
		linkedList.add(linkedList.size() / 2, value);
		return System.nanoTime() - startTime;
	}

	@Override
	public long removeLastFromCollection() {
		long startTime = System.nanoTime();

		try {
			linkedList.removeLast();
			return System.nanoTime() - startTime;
		} catch (NoSuchElementException | IndexOutOfBoundsException e) {
			Console.out("Size of collection " + getClass().getName() + " and using method "
					+ Thread.currentThread().getStackTrace()[1].getMethodName() + " is " + linkedList.size());
		}

		return 0;
	}

	public long removeFirstFromCollection() {
		long startTime = System.nanoTime();

		try {
			linkedList.removeFirst();
			return System.nanoTime() - startTime;
		} catch (NoSuchElementException | IndexOutOfBoundsException e) {
			Console.out("Size of collection " + getClass().getName() + " and using method"
					+ Thread.currentThread().getStackTrace()[1].getMethodName() + " is " + linkedList.size());
		}

		return 0;
	}

	public long removeMiddleFromCollection() {
		long startTime = System.nanoTime();

		try {
			linkedList.remove(linkedList.size() / 2);
			return System.nanoTime() - startTime;
		} catch (NoSuchElementException | IndexOutOfBoundsException e) {
			Console.out("Size of collection " + getClass().getName() + " and using method "
					+ Thread.currentThread().getStackTrace()[1].getMethodName() + " is " + linkedList.size());
		}

		return 0;
	}

	@Override
	public long searchElementByIndex(int searchIndex) {
		long startTime = System.nanoTime();
		try {
			linkedList.get(searchIndex);
			return System.nanoTime() - startTime;
		} catch (NoSuchElementException | IndexOutOfBoundsException e) {
			Console.out("Size of collection " + getClass().getName() + " and using method "
					+ Thread.currentThread().getStackTrace()[1].getMethodName() + " is " + linkedList.size());
		}
		return 0;
	}

	@Override
	public void performAction(int numberValue, int valueAdd, int valueSearch) {
		// LinkedList
		this.fillCollection(numberValue);
		// add
		long addTimeToTheEndTime = this.addToEndOfCollection(valueAdd);
		long addTimeToTheFrontTime = this.addToTheBeginningOfCollection(valueAdd);
		long addTimeToTheMiddleTime = this.addToMiddleOfCollection(valueAdd);
		// remove
		long removeLastTime = this.removeLastFromCollection();
		long removeFirstTime = this.removeFirstFromCollection();
		long removeMiddleTime = this.removeMiddleFromCollection();
		long searchTime = this.searchElementByIndex(valueSearch);

		Console.out("linkedList:\n" + "Search element: " + searchTime + "ns\n" + "Add to the end:\t"
				+ addTimeToTheEndTime + "ns\n" + "Add to the beginning:\t" + addTimeToTheFrontTime + "ns\n"
				+ "Add to the middle:\t" + addTimeToTheMiddleTime + "ns\n" + "Remove from the end:\t" + removeLastTime
				+ "ns\n" + "Remove from beggining:\t" + removeFirstTime + "ns\n" + "Remove from middle:\t"
				+ removeMiddleTime + "ns\n");
	}
}