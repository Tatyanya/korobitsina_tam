package by.epam.tam.previuos.task2.part2;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class ArrayListCollection extends AbstractCollection {
	private ArrayList<Integer> arrayList;

	public ArrayListCollection() {
		this.arrayList = new ArrayList<Integer>();
	}

	@Override
	public void fillCollection(int value) {
		for (int i = 0; i < value; i++) {
			Integer d = (int) (Math.random() * Integer.MAX_VALUE);
			arrayList.add(d);
		}
	}

	@Override
	public long addToEndOfCollection(int value) {
		long startTime = System.nanoTime();
		arrayList.add(value);

		return System.nanoTime() - startTime;
	}

	public long addToTheBeginningOfCollection(int value) {
		long startTime = System.nanoTime();
		arrayList.add(0, value);
		return System.nanoTime() - startTime;
	}

	public long addToMiddleOfCollection(int value) {
		long startTime = System.nanoTime();
		arrayList.add(arrayList.size() / 2, value);
		return System.nanoTime() - startTime;
	}

	@Override
	public long removeLastFromCollection() {
		long startTime = System.nanoTime();
		try {
			arrayList.remove(arrayList.size() - 1);
			return System.nanoTime() - startTime;
		} catch (NoSuchElementException | IndexOutOfBoundsException e) {
			Console.out("Size of collection " + getClass().getName() + " and using method "
					+ Thread.currentThread().getStackTrace()[1].getMethodName() + " is " + arrayList.size());
		}
		return 0;
	}

	public long removeFirstFromCollection() {
		long startTime = System.nanoTime();
		try {
			arrayList.remove(0);
			return System.nanoTime() - startTime;
		} catch (NoSuchElementException | IndexOutOfBoundsException e) {
			Console.out("Size of collection " + getClass().getName() + " and using method "
					+ Thread.currentThread().getStackTrace()[1].getMethodName() + " is " + arrayList.size());
		}
		return 0;

	}

	public long removeMiddleFromCollection() {
		long startTime = System.nanoTime();

		try {
			arrayList.remove(arrayList.size() / 2);
			return System.nanoTime() - startTime;
		} catch (NoSuchElementException | IndexOutOfBoundsException e) {
			Console.out("Size of collection " + getClass().getName() + " and using method "
					+ Thread.currentThread().getStackTrace()[1].getMethodName() + " is " + arrayList.size());
		}

		return 0;

	}

	@Override
	public long searchElementByIndex(int searchIndex) {
		long startTime = System.nanoTime();
		try {
			arrayList.get(searchIndex);
			return System.nanoTime() - startTime;
		} catch (NoSuchElementException | IndexOutOfBoundsException e) {
			Console.out("Size of collection " + getClass().getName() + " and using method "
					+ Thread.currentThread().getStackTrace()[1].getMethodName() + " is " + arrayList.size());
		}
		return 0;

	}

	@Override
	public void performAction(int numberValue, int valueAdd, int valueSearch) {
		// ArrayList
		this.fillCollection(numberValue);
		// add
		long addTimeToTheEndTime = this.addToEndOfCollection(valueAdd);
		long addTimeToTheFrontTime = this.addToTheBeginningOfCollection(valueAdd);
		long addTimeToTheMiddleTime = this.addToMiddleOfCollection(valueAdd);
		// remove
		long removeFirstTime = this.removeFirstFromCollection();
		long removeLastTime = this.removeLastFromCollection();
		long removeMiddleTime = this.removeMiddleFromCollection();
		long searchTime = this.searchElementByIndex(valueSearch);

		Console.out("arrayList:\n" + "Search element: " + searchTime + "ns\n" + "Add to the end: " + addTimeToTheEndTime
				+ "ns\n" + "Add to the beginning: " + addTimeToTheFrontTime + "ns\n" + "Add to the middle: "
				+ addTimeToTheMiddleTime + "ns\n" + "Remove from the end: " + removeLastTime + "ns\n"
				+ "Remove from begining: " + removeFirstTime + "ns\n" + "Remove from middle: " + removeMiddleTime
				+ "ns\n");
	}
}
