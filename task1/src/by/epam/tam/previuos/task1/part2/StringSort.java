package by.epam.tam.previuos.task1.part2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StringSort {
	private List<String> list;
	
	public StringSort() {
		list = new ArrayList<>();
	}

	public void parse(String str) {
		if (str.isEmpty()) {
			throw new IllegalArgumentException("Empty string");
		}
		list = Arrays.asList(str.split("\\s+"));

	}

	public void sortByVowels() {
		sort(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return getVowelsCount(o2) - getVowelsCount(o1);
			}
		});
	}

	private void sort(Comparator<String> comparator) {
		Collections.sort(list, comparator);
	}

	private int getVowelsCount(String input) {
		int count = 0;

		for (int i = 0; i < input.length(); i++) {
			Character currentChar = Character.toLowerCase(input.charAt(i));
			if (currentChar.equals('a') || currentChar.equals('e') || currentChar.equals('i') || currentChar.equals('o')
					|| currentChar.equals('u') || currentChar.equals('\u0430') || currentChar.equals('\u043e')
					|| currentChar.equals('\u0443')	|| currentChar.equals('\u044f') || currentChar.equals('\u0435')
					|| currentChar.equals('\u044e')	|| currentChar.equals('\u044d') || currentChar.equals('\u044d')) {
				count++;
			}
		}

		return count;
	}

	@Override
	public String toString() {
		String result = "";
		for (String element : list) {
			result += result.isEmpty() ? element : ", " + element;
		}
		return result;
	}

}
