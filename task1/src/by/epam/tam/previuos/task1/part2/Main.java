package by.epam.tam.previuos.task1.part2;

public class Main {

	private static String str;

	public static void main(String[] arg) {

		Console console = new Console();
		StringSort strSort = new StringSort();

		System.out.println("Sort list or array by vowels of consonants in string.");
		
		str = console.getAction();
		
		strSort.parse(str);
		strSort.sortByVowels();
		
		System.out.println("String after sorting\n" + strSort);

	}
}