package by.epam.tam.previuos.task1.part1;

public class CalcMain {
	public static void main(String[] arg) {
		Calc calculator = new Calc();
		Parser stop = new Parser();

		double temp, value, result;
		String action;

		System.out.println(
				"Calculator works with integer digits and simple actions: -, +, /, *. Enter \"=\" to get result");

		while (true) {
			temp = Parser.getValue();
			action = Parser.getAction();
			while (!action.equals("=")) {
				value = Parser.getValue();
				result = calculator.calculate(temp, action, value);
				temp = result;
				action = Parser.getAction();
			}
			;
			System.out.println("result is " + temp);

			if (!stop.getFinishFlag()) {
				break;
			}

		}
		System.out.println("Program is stopped");

	}
}
