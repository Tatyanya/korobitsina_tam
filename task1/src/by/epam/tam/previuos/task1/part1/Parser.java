package by.epam.tam.previuos.task1.part1;
import java.util.Scanner;

public class Parser {

	private static Scanner in;

	public static double getValue() {
		in = new Scanner(System.in);
		double value;
		while (true) {
			try {
				System.out.println("Enter a value");
				value = in.nextDouble();
				break;
			} catch (Throwable exc) {
				System.out.print("Incorrect value is entered. Try again. ");
				in.nextLine(); 
			}
			;
		}
		in.nextLine(); 
		return value;
	}

	public static String getAction() {
		String action;
		do {
			in = new Scanner(System.in);
			System.out.println("Enter an action: * / + - or =");
			action = in.nextLine();
			if (action.equals("+") || action.equals("-") || action.equals("*") || action.equals("/")
					|| action.equals("=")) {
				break;
			} else {
				System.out.print("Incorrect operation. ");
			}
		} while (true);
		return action;
	}

	public boolean getFinishFlag() {
		in = new Scanner(System.in);
		System.out.println("Enter \"y\" if you want to continue or enter another key to stop");
		String answer = in.nextLine();
		if (answer.equalsIgnoreCase("y")) {
			return true;
		} else {
			return false;
		}
	}
}
