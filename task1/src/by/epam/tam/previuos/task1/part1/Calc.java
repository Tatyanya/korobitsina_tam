package by.epam.tam.previuos.task1.part1;

public class Calc {
	
	public double calculate (double value1, String action, double value2) {
		double result = 0;
	
		try{
				switch (Operations.fromString(action))
					{
						case ADD:
							result = value1 + value2;
							break;
						case SUBTRACT:
							result = value1 - value2; 
							break;			
						case MULTIPLY:
							result = value1 * value2; 
							break;	
						case DIVIDE:
							result = value1 / value2;
							break;		
						default:
							new IllegalArgumentException("Incorrect operation");
					}
				}
				catch (ArithmeticException exc){
					System.out.println("Can't divide by zero! Use another value");
			}	
		return result;
	}
}