package by.epam.tam.previuos.task1.part1;

public enum Operations {
ADD("+"), SUBTRACT("-"), DIVIDE("/"), MULTIPLY("*");
	
	private String op;
	
	private Operations(String op) {
		this.op = op;
	}
	public String getValue() {
		return op;
	}
	  public static Operations fromString(String operation) {
	        for (Operations operator : Operations.values()) {
	            if (operator.getValue().equals(operation)) {
	                return operator;
	            }
	        }
	        throw new IllegalArgumentException("Incorrect operation");
	    }
}
